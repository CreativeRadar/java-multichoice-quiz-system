package test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.*;

public class QuizTest {


    int numOfQuestions =4;
    String difficultyLevel = "HARD";


    @Test
    public void startQuiz() {

        try (Stream<String> stream = Files.lines(Paths.get("Question_Bank.csv"))) {
            {
                stream.filter(p ->
                        p.contains(difficultyLevel))
                        .limit(numOfQuestions)
                        .forEach(System.out::println);

            }

        }catch(IOException e){
                e.printStackTrace();
            }

    }


    @Test
public void testIfQuestionsAreSelectedRandomly() {
        try (Stream<String> stream = Files.lines(Paths.get("Question_Bank.csv"))) {
            {
                stream.limit(numOfQuestions)
                        .collect(Collectors.collectingAndThen(Collectors.toList(), collected -> {
                         Collections.shuffle(collected);
                            return collected.stream();
                }))
                        .forEach(System.out::println);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testIfQuestionsAreSplitIntoArray() {
        try (Stream<String> stream = Files.lines(Paths.get("Question_Bank.csv"))) {
            {
                stream.limit(numOfQuestions)
                        .collect(Collectors.collectingAndThen(Collectors.toList(), collected -> {
                            Collections.shuffle(collected);
                            return collected.stream();
                        }))
                        .map(p -> p.split(","))
                        .forEach(p -> {
                            for (int i = 0; i <= p[i].length()-2; i++) {
                                System.out.println(p[i]);
                            }
                        });

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

