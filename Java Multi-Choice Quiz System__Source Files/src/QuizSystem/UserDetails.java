package QuizSystem;


import java.io.*;


public class UserDetails {

public static void saveUserDetails(String userName,int numOfQuestions, String difficultyLevel, int score, String path){
    try{
        FileWriter fileWriter = new FileWriter(path,true);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        PrintWriter printWriter = new PrintWriter(bufferedWriter);

        printWriter.println(userName + "|" + numOfQuestions + "|" + difficultyLevel + "|" +"Score:" +score);
        printWriter.flush();
        printWriter.close();

      System.out.println("Record Sucessfully Saved\n");
    }
    catch(Exception E)
    {
        System.out.println("Write Operation Failed");
    }
}


}

/*
REFERENCES

 O'Didily, M.2017.Writing to a csv file in java.Available at:https://www.youtube.com/watch?v=lp0xQXUEw-k [Accessed: 17 January 2019]

*/
