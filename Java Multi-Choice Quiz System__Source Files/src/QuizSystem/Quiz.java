package QuizSystem;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Quiz {

    String userName;
    int numOfQuestions;
    String difficultyLevel;

    Scanner userAnswer = new Scanner(System.in);
    int points = 0;

    void startQuiz() {

        // Set Username
        System.out.println("Please Enter your username");
        Scanner sc = new Scanner(System.in);
        userName = sc.nextLine();

        // Set Number of Questions
        System.out.println("Enter number of questions(1-10)");
        Scanner sc1 = new Scanner(System.in);
        numOfQuestions = sc1.nextInt();

        // Set Difficulty Level
        System.out.println("Set difficulty level(EASY, MEDIUM, HARD)");
        Scanner sc2 = new Scanner(System.in);
        difficultyLevel = sc2.nextLine();

        /*
           Load Questions From Question Bank CSV File into stream
           based on number of questions set and difficulty level
        */

        String questionBank = "Question_Bank.csv";
        try (Stream<String> stream = Files.lines(Paths.get(questionBank))) {
            {

                stream.filter(p ->
                        p.contains(difficultyLevel))
                        .limit(numOfQuestions)

                        // To display random Questions: Collect stream , shuffle, and return stream
                        .collect(Collectors.collectingAndThen(Collectors.toList(), collected ->{
                            Collections.shuffle(collected);
                            return collected.stream();
                        }))

                        // Format display: split each item into a stream of array of strings
                        .map(p -> p.split(","))


                        .forEach(p -> {

                            /* Display all items in each CSV Line except the last two
                              (Difficulty level & Correct Answer)
                            */
                            for (int i = 0; i <= p[i].length()-2; i++) {
                                System.out.println(p[i]);
                            }

                            // Retrieve user answer and check if correct


                            String answer = userAnswer.nextLine();
                            if (answer.equals(p[6])) {
                                points +=1;
                                System.out.println("correct\n" + "Score =" + " "+ points+ "\n" );
                            }else {
                                System.out.println("wrong! The Correct answer is"+ " "+ p[6]);
                            }
                                }
                        );
                System.out.println(userName +" "+"Your total score is:"+ " "+ points);

            }

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }



}

/*

REFERENCES
daShader.2016.How to get random objects from a stream.Available at:https://stackoverflow.com/questions/31703226/how-to-get-random-objects-from-a-stream [Accessed: 16 January 2019]



Questions with difficulty levels "Medium" and "Hard" were adopted from:

coding compiler.2019.60 Java Multiple Choice Questions And Answers 2019.Available at:https://codingcompiler.com/java-multiple-choice-questions-answers/[Accessed: 16 January 2019]

*/
