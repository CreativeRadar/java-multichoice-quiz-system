package QuizSystem;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class LeaderBoard {

    public void showLeaderBoard()

    {

        String record = "Users.csv";
        try (Stream<String> stream = Files.lines(Paths.get(record))) {
            {

                stream.forEach(System.out::println);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
