package QuizSystem;


public class Main {

    public static void main(String[] args) {

        //Start Quiz
        Quiz quiz = new Quiz();
        quiz.startQuiz();

       //Write User Details to CSV File
        String userName = quiz.userName;
        int numOfQuestions = quiz.numOfQuestions;
        String difficultyLevel = quiz.difficultyLevel;
        int score = quiz.points;
        String path = "Users.csv";

        UserDetails sb = new UserDetails();
        sb.saveUserDetails(userName,numOfQuestions,difficultyLevel,score,path);

        // Display Leader Board
        LeaderBoard leaderBoard = new LeaderBoard();
        leaderBoard.showLeaderBoard();







    }
}