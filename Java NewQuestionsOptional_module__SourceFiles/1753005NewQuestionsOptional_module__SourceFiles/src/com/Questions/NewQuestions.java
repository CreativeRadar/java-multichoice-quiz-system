package com.Questions;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;

public class NewQuestions {


        public static void addNewQuestion(String question,String option1,
                                       String option2, String option3,
                                       String option4, String difficultyLevel,
                                       String correctAnswer, String path){
            try{
                FileWriter writer = new FileWriter(path,true);
                BufferedWriter bWriter = new BufferedWriter(writer);
                PrintWriter pWriter = new PrintWriter(bWriter);

                pWriter.println(question + "," + option1 + "," + option2 + "," +
                                option3 + ","+ option4 + "," + difficultyLevel +
                                "," + correctAnswer);
                pWriter.flush();
                pWriter.close();

                System.out.println("Record Sucessfully Saved\n");
            }
            catch(Exception E)
            {
                System.out.println("Write Operation Failed");
            }
        }


    }

/*
REFERENCES

 O'Didily, M.2017.Writing to a csv file in java  https://www.youtube.com/watch?v=lp0xQXUEw-k.Available at:https://www.youtube.com/watch?v=lp0xQXUEw-k [Accessed: 17 January 2018]

*/