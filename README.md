# Java Multichoice Quiz System

A Java based multiple choice quiz system where a user can select the difficulty level of the quiz as well as the number of questions and take the quiz.
The results are displayed at the end of the quiz as well as a leader board.